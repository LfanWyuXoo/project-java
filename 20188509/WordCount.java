package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class WordCount {

	 public static void readTxtFile(String filePath){
	        try {
	                String encoding="GBK";
	                File file=new File(filePath);
	                int linenum=0;
	                if(file.isFile() && file.exists()){ //判断文件是否存在
			        	InputStreamReader read = new InputStreamReader(
						new FileInputStream(file),encoding);//考虑到编码格式
			        	ArrayList<String>list = new ArrayList<>();
			    		//创建高效字符缓冲输入流对象,并和data.txt文件关联
			    		BufferedReader br = new BufferedReader(new FileReader(file));
	                    String lineTxt = null;
	                  //循环读取每一行
			    		while(null!=(lineTxt = br.readLine())) {
			    			//将读到的每一行信息存入到list集合中
			    			list.add(lineTxt);
			    			if(!lineTxt.equals("") )
			                {
			                    linenum++;
			                }
			    		}
			    		
			    		int charnum=0;
			    		char[] ch = list.toString().toCharArray();
			    		for(int i = 0; i < ch.length; i++) {
			    		if(ch[i] >= 0 && ch[i] <= 127) {
			    		charnum++;
			    		}
			    		}
	        }else{
	            System.out.println("找不到指定的文件");
	        }
	        } catch (Exception e) {
	            System.out.println("读取文件内容出错");
	            e.printStackTrace();
	        }
	     
	    }
	     
	    public static void main(String argv[]){
	        String filePath = "L:\\Apache\\htdocs\\res\\20121012.txt";
//	      "res/";
	        readTxtFile(filePath);
	    }

}
